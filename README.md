# Использование библиотеки TestContainers
### Примеры 2 интеграционных тестов 

1. Тест для JDBC репозитория c использованием Testcontainers (StudentJdbcRepositoryTest)
2. Тест для REST контроллера с использованием MockMvc и Testcontainers (StudentRestControllerIntegrationTest)
