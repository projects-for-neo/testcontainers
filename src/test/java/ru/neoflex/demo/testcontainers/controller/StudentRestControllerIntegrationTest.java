package ru.neoflex.demo.testcontainers.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import ru.neoflex.demo.testcontainers.config.TestConfig;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfig.class)
@AutoConfigureMockMvc(printOnlyOnFailure = false)
class StudentRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllStudents_ReturnsStudentsList() throws Exception {
        // when
        this.mockMvc.perform(get("/api/students/"))
                // then
                .andExpect(status().isOk());
    }

    @Test
    public void saveStudent_ReturnsPersistedStudend() throws Exception {
        // when
        this.mockMvc.perform(post("/api/students/")
                .param("name", "Ivan Ivanov")
                .param("age", "20")
                .param("marks", "4.0"))
                .andExpect(status().isOk());
    }
}

