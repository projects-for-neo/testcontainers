package ru.neoflex.demo.testcontainers.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import ru.neoflex.demo.testcontainers.model.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers
@Slf4j
class SpringJdbcStudentRepositoryTest {

    @Container
    private final PostgreSQLContainer<?> POSTGRESQL_CONTAINER =
            new PostgreSQLContainer<>("postgres:11")
                    .withInitScript("testdb.sql");

    private SpringJdbcStudentRepository repository;

    @BeforeEach
    void setUp() {
        var hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(POSTGRESQL_CONTAINER.getJdbcUrl());
        hikariConfig.setUsername(POSTGRESQL_CONTAINER.getUsername());
        hikariConfig.setPassword(POSTGRESQL_CONTAINER.getPassword());
        var dataSource = new HikariDataSource(hikariConfig);
        repository = new SpringJdbcStudentRepository(new StudentSimpleJdbcInsert(dataSource),
                new JdbcTemplate(dataSource), StudentRowMapper.getInstance());
    }

    @Test
    void findAllStudents_ReturnsStudentsList() {
        // when
        var students = repository.findAllStudents();

        // then
        assertNotNull(students);
        assertEquals(2, students.size());
        assertEquals(new Student(1, "John Doe", 21, 3.55),
                students.get(0));
    }

    @Test
    void persistStudent_ReturnsGeneratedId( ){
        // given
        var student = new Student("Ivan Petrov", 20, 3.55);

        // when
        var newId = repository.persistStudent(student);

        // then
        assertEquals(3, newId);
    }
}

