package ru.neoflex.demo.testcontainers.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import ru.neoflex.demo.testcontainers.repository.StudentRepository;
import ru.neoflex.demo.testcontainers.repository.StudentRowMapper;
import ru.neoflex.demo.testcontainers.repository.StudentSimpleJdbcInsert;
import ru.neoflex.demo.testcontainers.repository.SpringJdbcStudentRepository;

import javax.sql.DataSource;

@TestConfiguration
public class TestConfig {

    @Bean(initMethod = "start", destroyMethod = "stop")
    public JdbcDatabaseContainer<?> jdbcDatabaseContainer() {
        return new PostgreSQLContainer<>("postgres:11")
                .withInitScript("testdb.sql")
                .waitingFor(Wait.forListeningPort());
    }

    @Bean
    public DataSource dataSource(JdbcDatabaseContainer<?> jdbcDatabaseContainer) {
        var hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(jdbcDatabaseContainer.getJdbcUrl());
        hikariConfig.setUsername(jdbcDatabaseContainer.getUsername());
        hikariConfig.setPassword(jdbcDatabaseContainer.getPassword());

        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public StudentRepository studentRepository(DataSource dataSource) {
        return new SpringJdbcStudentRepository(new StudentSimpleJdbcInsert(dataSource),
                new JdbcTemplate(dataSource), StudentRowMapper.getInstance());
    }
}
