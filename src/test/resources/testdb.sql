create schema test;

create table test.student
(
    id       serial primary key,
    name     varchar(255) not null unique,
    age      numeric,
    marks    numeric
);

insert into test.student (name, age, marks)
values ('John Doe', 21, 3.55),
       ('Jane Doe', 17, 4.75);