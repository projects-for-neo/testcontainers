package ru.neoflex.demo.testcontainers.repository;

import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public final class StudentSimpleJdbcInsert extends SimpleJdbcInsert {

    public StudentSimpleJdbcInsert(DataSource dataSource) {
        super(dataSource);
        this.withSchemaName("test")
                .withTableName("student")
                .usingGeneratedKeyColumns("id")
                .usingColumns("name", "age", "marks")
                .compile();
    }
}

