package ru.neoflex.demo.testcontainers.repository;

import lombok.NonNull;
import ru.neoflex.demo.testcontainers.model.Student;

import java.util.List;


public interface StudentRepository {

    List<Student> findAllStudents();

    public int persistStudent(@NonNull Student student);

}
