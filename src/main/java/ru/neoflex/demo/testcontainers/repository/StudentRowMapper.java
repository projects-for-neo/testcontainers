package ru.neoflex.demo.testcontainers.repository;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Component;
import ru.neoflex.demo.testcontainers.model.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StudentRowMapper implements RowMapper<Student> {

    private static final StudentRowMapper INSTANCE = new StudentRowMapper();

    public static StudentRowMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public Student mapRow(@NonNull ResultSet rs, int rowNum) throws SQLException {
        return new Student(rs.getInt("id"), rs.getString("name"),
                rs.getInt("age"), rs.getDouble("marks"));
    }
}
