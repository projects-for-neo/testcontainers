package ru.neoflex.demo.testcontainers.repository;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsertOperations;
import org.springframework.stereotype.Repository;
import ru.neoflex.demo.testcontainers.model.Student;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Repository
public class SpringJdbcStudentRepository implements StudentRepository {

    @NonNull
    private final SimpleJdbcInsertOperations insertOperations;

    @NonNull
    private final JdbcOperations jdbcOperations;

    @NonNull
    private final RowMapper<Student> studentRowMapper;

    @Override
    public List<Student> findAllStudents() {
        return this.jdbcOperations.query("select * from test.student", this.studentRowMapper);
    }

    @Override
    public int persistStudent(@NonNull Student student) {
        return this.insertOperations.executeAndReturnKey(Map.of(
                "name", student.getName(),
                "age", student.getAge(),
                "marks", student.getMarks()
        )).intValue();
    }
}
