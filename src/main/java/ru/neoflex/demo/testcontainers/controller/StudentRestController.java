package ru.neoflex.demo.testcontainers.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ru.neoflex.demo.testcontainers.model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.neoflex.demo.testcontainers.repository.StudentRepository;

import java.util.List;

@RestController
@RequestMapping("/api/students/")
@RequiredArgsConstructor
public class StudentRestController {

    @NonNull
    private final StudentRepository studentRepository;

    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents() {
        return ResponseEntity.ok(studentRepository.findAllStudents());
    }

    @PostMapping
    public ResponseEntity<Student> saveStudent(String name, int age, double marks) {
        var student = new Student(name, age, marks);
        student.setId(this.studentRepository.persistStudent(student));
        return ResponseEntity.ok(student);
    }
}

